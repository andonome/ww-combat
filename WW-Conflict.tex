\documentclass[english,HQ,Vampire]{WoDTeX}

\input{preamble.tex}

\setcounter{tocdepth}{1}
\setcounter{secnumdepth}{0}
\setcounter{chapter}{-1}
\begin{document}

\FrontCover{{Revised Rules}}{\begin{tikzpicture}
\node [draw,rounded corners, fill=black!25] {\includegraphics[width=.9\textwidth]{Backgrounds/WoDTeXLogo}};
\end{tikzpicture}}

\clearpage

\CreditsPage{Ghosty}{Mc}{Ghostface}{Many thanks to Lukas for making this \LaTeX~ document.}

\twocolumn

\section*{Core Concepts}
The system aims to provide a single, unifying conflict resolution system, whether characters are engaged in arguments, plots, crafting traps, or pulling each other apart.  Different types of combat have special rules, but the core is always the same.  Two opponents face off against each other and both roll the same \textit{Attribute + Ability}, but their own Bonuses.\footnote{`Bonuses' refers to anything which can give a character bonus dice, such as weapons or a Background.}

In essence, this is a new system only for \textit{Extended and Resisted} actions.

It's easy to understand after a few basic examples:

\WoDFrame{
The old combat system has been completely left behind here, for an entirely new start.  There is no more initiative or soak.  The only thing left of the rules is your basic Attribute + Ability rolls.}

\subsubsection{Examples}

\textit{A janitor sees the coterie tear someone apart on CCTV.  The phone lines have already been cut so he flees the building and the coterie give chase.}

\begin{itemize} 

	\item {The Storyteller picks \emph{Intelligence} as the janitor's Trait of choice while the characters choose \emph{Streetwise}.  All characters roll \emph{Intelligence + Streetwise}.}

	\item{Every success the coterie gain narrows where he might have fled to, which inflicts increasing penalties to his dicepool.}

\end{itemize}

\textit{Branden is trying to convince everyone that the Prince has been replaced by a Tzimisce who is simply wearing his face, but the prince publicly mocks him for it.}

\begin{itemize}

	\item{The Prince selects \emph{Empathy} and Brendan's selected Trait is \emph{Appearance}.  Both roll \emph{Appearance + Empathy}, but only the Prince gets to add his Status.}

	\item{The prince decides not to taunt Branden, but to simply offer his sincerest sympathies for the Malkavian's condition, and even the use of his own hunting grounds on the condition that Branden refrain from stirring up supernatural fervour among the kine there. Branden has lost no reputation, but is placed in a very difficult position after the entire court is focussed on his habit of holding seances.  He gains +2 difficulty to all further rolls.}

\end{itemize}

\textit{Jason's gone too far with his mocking tones, and has sent one of the Brujah into a frenzy.}

\begin{itemize}

	\item{As the Brujah has Initiated the attack, Jason decides on \emph{Strength} while the Brujah picks \emph{Brawl}.  Both roll \emph{Strength + Brawl}, but Jason at least has the forethought to pull his knife out to guard himself, so he gains +1 to the roll.}

	\item{Jason jumps back instead of using the dagger, buying himself half a moment to speak.  He immediately tries to use the Dominate to restrain the Brujah's beast.}

\end{itemize}


\subsection*{A New Trait Philosophy}Within these rules, Traits are viewed as 
battle-grounds rather than as pairs which can work in opposition; opponents do 
not roll using different Attributes and Abilities.  Characters use the Finance 
Knowledge to protect their own funds and bleed others.  Subterfuge measures how well characters lie and how well they spot lies.  Melee covers wielding a blade and dodging a blade's arc.  This philosophy doesn't have to be adhered to at any point other than in the arena of continuous conflict -- when characters have Extended and Resisted rolls.

\section{Rules}
We start with the approach.  Someone has to start a confrontation or it can 
never really begin.  The confrontation starts when someone makes a move -- running at an opponent, making a snide comment, trying to uncover another's secrets, et c.  Whoever initiated the action decides if this combat will be Physical, Social or Mental, and the engagement will use just those stats from then on.

Next, the defender selects the Attribute.  In this way, defenders can always make at least some adjustment to the arena.  If the attacker is beginning a Physical assault, the defender might select Dexterity as the Trait to show that this competition will begin with a chase, or at least involve some dodging.  Or if someone begins a social competition, the defender may select Wits to shift the ground onto quick thinking.

The initial attacker then seals the Traits in place by selecting a Ability.  If the defender is using Intelligence to hide from the police by leading them on a mad goose-chase of obvious, but false monetary leads, then the police chief can decide to use Finance as the Ability of choice to scope the kindred's financial footprint.  In any case, there will always be some combination of \emph{Attribute + Ability} which both participants must use.

Finally, the combatants each choose their own Bonus to use.  A journalist may push upon local Contacts in order to find a vampire's identity through a historical society.  The local anarchs may try to use the fact that they have a well-positioned haven as a bargaining chip to prevent war.

\subsection{Consequences}Both combatants roll their dice, and whoever gets the most successes wins.  Like any resisted roll, the winner only keeps the additional successes.  When you roll three successes and your opponent gets two, you only get to keep one, while if your opponent rolls five and you roll four, sie keeps only one.

Successes can be spent on the following effects:

\subsubsection{Damage} 

This is the meat of the system: every success the character inflicts can turn into a penalty to the opponent's dice pool.

When someone reaches a -5 penalty, or a large enough penalty to have a dice pool of 0, she is incapacitated.

\begin{itemize}
	\item{In Physical combat, this usually represents a wound, which can be healed in the usual way.}

	\item{In social situations, a penalty means that someone has lost face before a crowd.}

	\item{Mental tasks might concern uncovering or obfuscating knowledge.}

	\item{Characters can \emph{resist} rather than soak these penalties, ignoring a number equal to their Stamina, Status, or some other Trait.}

	\end{itemize}

\subsubsection{Positioning}

Your feet are important, whether your position in court or how far away you are from a nearby cliff. Combatants can elect to use successes to raise their opponent's difficulty by +1 per Success.

\begin{itemize}

	\item{In Physical Combat, poor positioning can represent being backed up against a cliff, having one's weapon controlled by an opponent, or slipping on ice.}

	\item{In Social Combat, characters might be forced to use a language they don't know well, or perhaps the social group has been reminded of the character's recent failure.}

	\item{In Mental combat, poor positioning might mean that a character has become inundated with other things to think about, or distracted by something irrelevant.}

\end{itemize}

\subsubsection{Recovery} 

Recovery is the flip-side of Positioning, and lets a character recover from \emph{all} penalties to difficulty.  Even if someone has spent 3 successes to give you a +3 to the difficulty you roll at, you only have to spend 1 success to recover from that penalty.

\textit{James is mocked publicly in court by the local harpy, inflicting a +1 penalty to the social action. The prince then reminds everyone of his recent failure, inflicting a further +2 penalty.  James reminds the court that this difficult task was ultimately the responsibility of the prince, so he loses the +2 penalty, but keeps the +1 penalty inflicted before.}

\subsubsection{Change Arena} refers to switching the Traits used in the Contest.  At the beginning of all conflicts a particular \emph{Attribute + Ability} pair lay out the arena in which the combat will take place.  By spending a single success any combatant can change one of these two Traits to another.

\textit{Adrian attempts to trick the nurse into giving him the prince's ghoul's medical records.  The Traits involves are \textit{Wits + Subterfuge}, but as the nurse wins the first conflict, she switches the conversation to what the records are for, focussing on the Medicine Ability, rather than Subterfuge.  The interaction is now based on Wits + Medicine, and Adrian doesn't have a leg to stand on.}

\subsubsection{Something else}

Characters can spend a single success to do any other action.

\begin{itemize}

	\item{A character with a gun might get off a single shot mid-brawl.}

	\item{Kindred might use a discipline.}

	\item{A security guard might activate a fire alarm.}

	\item{Move somewhere else.}

	\item{Any one action not clearly covered in previous rules -- even a one-off resisted roll -- can become a quick Left Turn.}

\end{itemize}

This `something else' can have a full roll, and perhaps even a resisted roll, so long as it is clearly one, self-contained, action.

\subsubsection{Spread the Damage}

Characters can take any effects and spread them to multiple opponents.  You can even combine these effects -- 3 successes could hit 2 opponents for 1 Damage and +1 difficulty each.

\subsection{Resistance \& Defensive Attributes}
Each type of combat has an Attribute which does not allow for Damage, but does allow characters to resist Damage.

\subsubsection{Physical Contests}

Stamina is used to resist Damage, so a character who receives 4 Damage, and has Stamina 2 would only mark down 2 Damage, as the other two was resisted.  Stamina can be used to wrestle opponents, putting them in a bad position, but can never inflict a penalty.

\subsubsection{Social Contests}

Appearance is used to ward off the penalties which might otherwise come with a bad reputation, but cannot cause that kind of reputation Damage.

\subsubsection{Mental Contests}

Wits is used for damage control, but can never inflict penalties on opponents.

\textit{Adrian is hit for 5 damage with a shovel by a member of the Sabbat.  His Stamina is 3 so he only receives a -2 penalty, but he will not be able to use his Stamina to resist anything else for the rest of the scene.}

\subsection{Botches}
The results of Botches lie with the Storyteller, but a good rule of thumb is to inflict +1 difficulty due to poor positioning.  This can be ignored by correcting one's positioning, as usual.

\subsection{Initiative \& Numbers}

The order in which people take actions is not important.  Start with the first player to speak, and if that fails, go in seating order.  Left of the Storyteller starts.

\subsection{Multiple Opponents}

Contests with groups mean everyone pairs off against each other.  Anyone can declare an attack on one other person.  If someone is not yet engaged in a combat, they engage at that point.

When joining a combat late, the initial combat process is maintained -- the defender still has the ability to determine the Attribute of the Arena, but the new attacker can determine the Skill used.

\begin{example}Adrien asks the Prince where he was last night -- it's a very public and pointed question after the Sabbat attack.  The Arena is Charisma + Empathy.

On the first round the Prince asks him how much information he thinks it would be intelligent to hand out, given the pressing times; there may be secrets that are best not known to anyone, and Adrien get a +2 penalty to all future difficulties, as he has to be careful about what he wants to ask, or look like he does not care what information comes out.

On the next round, the Nosferatu Primogen comes to his aid, stating that it is abnormal for a Prince, in a time of war, to disappear in a shroud of secrecy.  The Prince still has to use Charisma, but the Primogen decides to change the Arena to Etiquette.

The Primogen and Adrien roll again, gaining 6 and 2 successes respectively.  The Prince gets 5, so the Primogen has given him +1 difficulty as he's now in a difficult position, and the court can sense it.

\end{example}

\subsection{Interpretation}

Players are quite welcome to look at the rules in a mechanical way and simply think about what moves would win fastest.  However, they must also accept the responsibility for immediately giving an engaging and fitting description of what their action is.  Etiquette might be your highest Ability, but if it's not clear how exactly you can turn the crowd against a local harpy with polite phrases then you will have to find another Ability to use.  Alternatively, if you want to immediately drop to character and tell the crowd that you've seen the movements of the rats in this city, and \emph{know} that something dangerous lurks in the sewers, then perhaps you can use Animal Ken to push the crowd into your side of the debate. At all times, the group must agree that the interpretation makes sense and as usual the Storyteller has the final say.

Because more options are provided than simple damage, conflicts do not necessarily have to have long-term consequences.  Kindred who publicly humiliate the local prince might have a good laugh that night, but the prince shall remember.  If they simply position him such that he would prefer to accede to their demands rather than risk the loss of face which would come with being called out in front of the whole court then they have no need of inflicting permenent consequences.  Of course, the prince might not have the same attitude towards others\ldots

This system is rather more abstract than many, and the Players will simply need some understanding.  If they're engaged in a turf-war and end up using Empathy, what's to stop them using the Law Ability, and bringing the police into the field?  Why can't they do this, simply because they lost a roll?  A creative Storyteller can always find answers, but it's better if Players are on board from the beginning with the cost of a little abstraction.

\subsection{Timing \& Records}

Each type of Contest has a rate at which one can roll for an attack, and a rate at which the \textit{Resistance} regenerates (be it Stamina, Appearance, or Wits).

For example, Physical Contests have one roll per turn,\footnote{Meaning, every 3 seconds} while Social Contests usually have one roll per scene, and regenerate the Resistance Attribute (Appearance) only once per chapter.

\noindent\begin{tcolorbox}[tabularx={X|X|X},title=Contests]
	\textbf{Contest} & \textbf{Roll} & \textbf{Renewal} \\\hline\hline
	Physical & Turn & Scene \\\hline
	Social & Scene & Chapter \\\hline
	Mental & Chapter & Story \\\hline

\end{tcolorbox}



As you can see, Physical Contests are deadly and fast.  Social Contests can endure for a whole chapter, or story, as characters repeatedly jab at someone's reputation, humiliate them, or simply provide a better offer.  Meanwhile, Mental Contests can move through different stories.

\subsubsection{Tracking the Damage}

So many Contests happening all at once demand good tracking.  Players should have a good space on their character sheet (or its back) set out to track being part of umpteen different Contests.

Each Contest can have its own set of environmental variables -- one's reputation in Elysium does not necessarily reflect a reputation in a nearby criminal enterprise.  In fact, at a single time, a single character may be engaged in a Mental Contest with a local Primogen to gain power over a piece of turf, another to hide from a local Private Investigator, a Social Contest against two harpies, and a fight with a Gangrel.

\begin{example}

	\begin{tabular}{l|l}

		\textbf{Mental} & \\\hline

		Primogen: +1 diff &  3 Dam +1 diff \\

		PI: +1 diff 2 Dam & +1 diff \\

		\textbf{Social} & \\\hline

		Harpies: & +3 diff \\

	\end{tabular}


\end{example}

%\subsection{Healing}
%Physical Damage heals at the usual rate.  Social and Mental Damage can only be healed by spending time after a Contest, mending a broken reputation or putting plans and business empires back together.
%
%The Traits rolled are whatever Traits the Contest ended upon, with whatever difficulty the Contest ended on.
%
%\begin{example}
%
	%Jane's reputation as an artist had been crushed during the smear campaign in the newspapers.  She'd won the war, but hadn't come out looking pretty.  Many a night had to be spent picking up all the pieces of broken friendships, and getting new contacts in the scene.
%
	%Jane's Player had been fighting with Charisma + Empathy when the battle finally ended, at difficulty 7.  She takes time the next night to make yet another roll, and heals 2 of her 3 Damage on the first night, none on the second, and finally recoups her position properly on the third night.
%
%\end{example}
%
%These wounds from so many contests can fester for a long time.  However, when it comes to lost battles, such as when kindred fight and lose a friend, there is nothing to heal, so no rolls need to take place; the damage is done.

\section{Viscera}\label{visref}
As conflicts ensue, the Storyteller will tweak the rules to represent the best fit of the scene.  We'll fly over some basic standards in these sections covering Physical, Social and Mental challanges but individual stories will inevitably call for tweaks and modifications in order to best represent the challenges.  In such situations the Storyteller need only mention the rule modifications the moment before players make their decisions, though not necessarily before they have decided to enter a maelstrom.

\subsection{Physical Combat}
Physical combat has the fastest flow, resolving each action with in-game seconds rather than months.  It has immediate, devastating consequences, but need not always kill.

\subsubsection{Attributes}

The Strength Attribute covers sudden, devastating attacks such as stabs or strikes.  It also covers putting opponents off-balance with simple shoves and pushes.  Dexterity covers precise attacks such as eye-gouges or leg-sweeps.  Stamina covers more endurance-based attacks, such as grappling opponents.

\subsubsection{Abilities}
Brawl covers any combat without a weapon while Melee covers all combat where either opponent has a weapon.

At other times, a combat will suddenly switch when one opponent decides to change the Ability to Athletics and flee.

\subsubsection{Bonuses}
Weapons will grant the character the ability to damage and deflect damage.  Each one has its own advantages and disadvantages.  Of course, characters can ignore any disadvantages by simply dropping the weapon.  This costs nothing and does can be done before dice are rolled, but picking the weapon up again will require a success to be spent.

\vspace{.5cm}

\noindent\begin{tcolorbox}[tabularx={l|c|c|c|},title=Weapons]
		Weapon & Strength & Dexterity & Stamina \\\hline\hline
	
		Axe & +3 & +0 & -2 \\
		Knife & +1 & +1 & +1 \\
		Lead Pipe & +3 & -1 & +0 \\
		Sword & +2 & +1 & -1 \\

		\hline
	\end{tcolorbox}
	

\vspace{.5cm}

\subsubsection{Damage}

Combat inflicts penalties of any kind the player can think of.  If some player smashes in an oppenent's legs then the opponent receives a penalty to all actions involving moving, such as combat, but probably not shooting a gun.  Players might also cut an opponent's arm, which will give the opponent a penalty to shooting and attacking, but not running away.  A blow to the eyes might not entirely blind someone, but can impede sight-based actions such as attacking, shooting and fleeing (though none of those will be particularly affected if the characters begin fighting in the dark).

As with any facet of conflicts, players must put forward a fitting interpretation.  You might want to use Dexterity to put that Lasombra off-balance, raising the difficulty.  But if she's towering over you like a ghoulish octopus with four additional tenebrous arms then it's hard to see how a leg-sweep will accomplish that.

As usual, Damage can be healed by spending blood points after combat has ended.  Every blood point spent can heal a single Lethal penalty or 2 Bashing Damage penalties.

\subsubsection{Resistance}
People resist Damage with their Stamina.  As before, kine can only resist Bashing Damage, while kindred can also resist lethal Damage.

Each point of Stamina counts for two when resisting guns.

\subsection{Social Confrontations}

Social conflicts have been an age-old problem in RPGs.  If characters' words take precedence then that only leaves players with the permission to imagine that someone socially savvy is doing something while they sit a few abstractions removed.  If the players' words take prescedence then the characters' stats seem pointless.  Here, we leave the dice as something for the players to interpret.  When a high roll is achieved, characters speak well.  When they fail, players can still choose to speak, but will have to speak poorly.  The roleplaying comes in as a proper interpretation of the dice, hopefully in accordance with the characters' Demeanors.

\subsubsection{Attributes}
The nature of a converation can seriously restrict which Attributes make sense.  Shows of bravado to win popularity hardly merit a Manipulation roll, though trying to push at someone's desires almost certainly will.

Appearance is the Social equivalent of Wits.  Players should feel especially free to use it beyond simply seducing people -- our face and basic mannerisms are how people judge us before they know us, and a nice voice or confident words (however lacking in substence) can take people a long way.  When trying to show confidence before anyone really has a measure of a character's worth, or when conversational interlocutors throw shade on each other, characters use Appearance.

Charisma is better suited to longer term arguments or winning over friends.  Such conflicts include pulling in followers to a cause, or pushing characters to fear.

Manipulating opponent's means deflecting issues and rhetorical tricks.  When someone lowers the tone of a public debate with aspersions, muddying the water and constantly changing subject to distract onlookers and irritate the opponent, the entire debate suffers and opponents can feel forced to fight back with the same tactics.

Manipulation also best suits situations where characters argue one-on-one.  Arguments don't have to involve any shouting or outright confrontation -- sometimes people simply try to wear each other down with unkind (or perhaps overtly kind) words.

\subsubsection{Abilities}

Conversations might involve Empathy where people want to understand a crowd, or want to get under a rival's skin and see what makes hir itch.  Battles of lies, however, rely on Subterfuge -- both to spot and push a lie.  Once subterfuge has been chosen as an Attribute, the battleground can still consist in true statements individually, but little half-truths distort onlookers' perceptions, or seek to humiliate a rival publicly.

Of course these Abilities need not play out in a single scene.  Characters might use \emph{Charisma + Streetwise} in order to gain a reputation on the streets over some weeks.  Leaderships skills typically help nobody in the course of a conversation, they develop a relationship over the course of weeks or months.

Given how each Ability must be mirrored by each opponent, Intimidation no longer simply uses threats but also tracks a character's ability to see through intimidation tactics and ignore them, seeing where a threat is overstated, or where it might lie beyond the reasonable use of someone's time.

\subsubsection{Bonuses}
This is where Contacts, Allies, Herd, Influence and Status come into play.  Characters can threaten by referencing their gang friends, or build a reputation by pulling on Contacts.  They can use Allies to spread detailed half-truths or request a herd to act in unison.  Any Background which seems justifiable can be used, but backgrounds can only be referenced by Social Attributes -- at the end of the day characters must do most of their own legwork when it comes to social interaction.

\subsubsection{Damage}
The final result of poor social interaction is typically a loss of face among a particular group.  The local Elysium, or the local clubscene -- any group which knows the character can think less of hir.

Alternatively, with or without an audience, characters may simply pick at each others' psyche.  These close quarter discussions only affect people close to each other.  Character who attempt to shit-talk an elder will quickly just see the elder's indifferent back.  Jibes affect people when both have already opened themselves to each other.

When two characters agree to enter into such a psychological contest, the first to walk away loses a point of Willpower.  If the competition plays till the end then the winner may spend any additional successes -- i.e. successes required to lower the other to below nothing -- on lowering the opponent's Willpower.

\begin{example}

Thomas' mortal child has finally caught up with him.  She has no idea about his undeath of course, and things proceed poorly.  He tries to push her away to stop her investigation of every part of his life, but every failure from his mortal life is brought up until eventually he's worn down to nothing.

	\begin{comment}
	The conflict takes place with Manipulation + Empathy and once Thomas is worn down to 1 single die she scores 2 successes. She removes his last die, then removes a Willpower point, and then he must leave the conversation as he feels too worn out to continue.  This removes another Willpower point.
	\end{comment}

\end{example}

\subsubsection{Resistance}
Typically characters will resist the consequences of social conflicts with the Status background, though in special circumstances the Storyteller may allow others.  When people attempt to embarrass the kindred in front of hir herd, a larger herd will give more resistence to tuants and accusations, so that background can allow the kindred to ignore those penalties.

\subsubsection{Groups}
Arguing in groups is pointless unless they can coordinate well.  Any time 
groups engage in arguments, everyone can roll, but only the highest score 
takes effect.  When two groups argue to sway a crowd or sully a reputation, 
everyone rolls, but only one takes effect.

\subsection{Mental Challenges}
These conflicts typically last the longest of all.  Neonates and ancillae with powerful disciplines can make a fast splash, but the reason the elders always seem so clever is that all the dull kindred died long ago.

The downside of acting as part of a group is that any character who botches subtracts 1 success from the group's highest roll.  The best way to defame some position is to defend it poorly.

\subsubsection{Attributes}
Gathering a business empire or destroying another can use Intelligence, but could just as easily require Perception to pull apart the details hidden in the books.  Wits, while a naturally fast-paced Attribute, can still allow someone to coordinate a campaign of fast-paced changes.  Building some empires cannot be planned -- pushing for popularity on the music scene or getting through a roadtrip safely, and without perishing in the sunlight, can all use Wits.  And equally, evading persuit on that road or avoiding detection as kindred while on stage will test one character's Wits against another's.

\subsubsection{Abilities}
As usual -- Traits form battle-grounds.  The Security Skill allows someone to protect or invade a stronghold.  The Computing Knowledge allows server infiltration.  Though as usual, Traits can change, and a hack-attack can quickly turn from \emph{Intelligence + Computing} into \emph{Intelligence + Finance} when the core components of the business simply vanish from the servers as funds get liquidated to buy other assets.

\subsubsection{Bonuses}
Just as with Social Traits, any Background which fits can see use.  Additionally, computing characters might use an advanced computer, or someone hunting kindred could purchase heat-sensitive filming equipment.  Any piece of equipment can be represented with a dice-pool bonus, like a weapon.  A computer with a fast processor might grant +3 to Wits-based computing actions, while having good quality programs might better allow someone to crack a password.

\subsubsection{Damage}
Mental challenges set their own stakes.  Sometimes characters only want to win a game of chess.  At other times, they plan to destroy another's herd by making it tear itself apart.  Of course, using a Bonus also means risking it.  Using one's Retainers risks their death.  Using one's Contacts to gather information means exposing them.  Once the challenge is lost, any successes beyond those required to reduce an opponent to nothing can destroy a single Background point.  Scoring 3 successes on an opponent who was reduced to only 1 die means ripping out 2 points worth of a background as Retainers die, or a Herd abandons their leader.  Alternatively, Mental challenges can deal in information.  Characters might want to know a serial killer's identity, or discover why the Nosferatu are so scared yet will talk to no-one.

\subsubsection{Resistance}
Any single appropriate Trait may allow characters to ignore penalties so long as it's appropriate.  When engaged in hacker warfare, characters might have a computer which grants +3 to resisting infiltration attempts.  A character's Haven rating may also allow it to resist discovery, due to a good location.  The backgrounds used may or may not be the same as Bonuses that characters use when engaged in conflicts -- the final decision rests with the Storyteller.

\subsubsection{Groups}
Groups of characters can act together just as with Social conflicts -- everyone might roll, but only the highest roll counts.

\section{Storytellers}
Since Storytellers know the player characters' Traits and those of their opponents, they retain a distinct advantage.  When players want to know which Traits will give them the best advantage, they must simply guess at their opponents' weaknesses.  So, like the dealer in Pontoons, Storytellers live by a simple code -- select the highest possible Trait.  Sometimes that elder would be better off using hir Subterfuge score of 3 because the insolent character trying to undermine hir plans only has Subterfuge 0, but if the elder's Empathy Talent is 4 then Empathy must be selected, even if the character's Empathy is also 4.  If two Traits are tied for the highest, the Storyteller may pick one.

Of course, not all Traits are appropriate to all attacks or defences, so this must be considered good practice rather than a rule.

%\subsection{Down the Ages}
% Things to do over years.
% Gain Backgrounds
% 	Herd, haven, domain, revenants, herd.
%	Attack
% Over the years, people can gain a lot.  During downtime, each block of time allows a single roll be made.  If resources are uncontested, then the character can simply roll, and gain some number of dots.  If the resources are contested, then rolls must be made.
%
% Perhaps people with more things can have more years to gain.
% 
% Perhaps both in a fight choose their timing, longer or shorter.  Shortest wins?
% 

\section{Disciplines}
All these changes to Combat warrant some changes to Disciplines, though I hope to alter as little as possible of the spirit of the rules.

\subsection{Celerity}
Celerity now adds dice to a character's dicepool once activated.  Characters who spend a blood point gain the bonus to any Physical action.

\subsection{Dementation}
Each level of Dementation remains as it was, but the mechanical results differ, or perhaps complement the existing rolls.  Eyes of Chaos (level 3) can still allow the Malkavian to see patterns in large events, but while actively engaged in Extended and Resisted rolls, the difficulty can increase or reduce randomly.

\begin{enumerate}
\item If this power succeeds, the Malkavian can lock the competition into the Traits as they stand.  If the conflict involves \emph{Charisma + Academics} then it will remain with those Traits, and no number of successes can change the nature of this competition.  It is typically not possible to lock in Physical combat, because wrestling with someone while whispering little words of madness under the stated and more obvious words is not possible, but the Storyteller is free to add exceptions.
\item The target gains +1 difficulty to all rolls involving a Social or Mental Attribute of the Malkavian's choice.
\item The Malkavian gains +2 difficulty to all Mental rolls where sie uses an even number of dice and -1 difficulty when sie uses an odd number of dice.
\item The target gains a +2 difficulty penalty to all Social or Mental rolls with an even number of dice.
\item The target gains a +2 difficulty penalty to all Social or Mental rolls with an odd number of dice and -1 difficulty when using an even number of dice.
\end{enumerate}

\subsection{Presence}
Presence now functions only as a form of social Potence.  If the character wins a competition, inflicting penalties on an opponent, sie gains a number of additional successes equal to hir Presence Discipline.  As usual, if no successes are scored then no bonus applies.  Presence has no other effect.
  
\subsection{Fortitude} Fortitude allows a character to ignore a number of Damage penalties to dice equal to the character's Fortitude score.  This counts for every instance of incoming Damage, unlike Stamina, which retains that Damage until the end of the scene but allows the penalty to be ignored.  Characters with Fortitude 1 can receive three -2 penalties and be left with only a -3 penalty in total.
\section{Special Manouevres}
\subsubsection*{Firearms}
Anyone shooting a gun into a melee is simply shooting as normal.  And as normal, botches can get friends killed.  Anyone inside combat simply needs to expend a success in order to fire the gun.  The first character rolls, the second rolls to Dodge with Dexterity + Athletics, and if a single success is scored then the gun's total Damage applies to the enemy, along with +1 for each success scored.  If a gun's Damage is +3 and someone scores 2 successes then the total Damage is 5 Lethal for mortals or 5 Bashing for kindred.  

\subsubsection*{Ambushes}
An ambushing character rolls, usually with \emph{Intelligence + Stealth} or \emph{Dexterity + Stealth} and the opposition rolls \emph{Perception + Stealth}.  For every success the aggressor scores above the opponent, sie starts with 1 combat success which can be applied in the normal way in a free `round 0'. The two begin combat as normal, and then the aggressor can apply a dicepool penalty, switch Traits, et c.

\begin{example}
The local gang are waiting round the corner for Oscar.

	\begin{comment}
		The leader rolls Intelligence + Stealth to hide in the abandoned schoolhouse and acheives 3 successes.  Oscar rolls Perception + Alertness and achieves a single success.

		In the approach, Oscar selects Stamina as his Attribute of choice to block the incoming attacks, while the gang members select Melee to lay into him with their clubs, hammers and pipes.  Those without weapons still have to use the Melee skill in order to coordinate around their comrades.

		The highest result from the gang is 4 successes, so Oscar receives 4 lethal Damage, and with a Stamina of 2, that leaves him with a -2 penalty to all actions.  Combat begins as usual, and Oscar's not starting in good shape.
	\end{comment}

\end{example}


\subsubsection*{Pulling a Weapon}
Whether someone pulls a weapon out or someone new joins a fight with a weapon in hand, all characters involved in the combat must immediately switch to using Melee rather than Brawl if they were using Brawl before.  Of course, the combat can still change back to Brawl, representing a fight which has become so close-quartered that characters simply have no room to manoeuvre their weapon.

\subsubsection*{Targeted Shots}
Characters using Dexterity to attack can add +1 difficulty to their roll at the start of the round in return for +1 Success if they hit, or +2 Difficulty in return for +2 Successes.

\subsubsection*{Fangs}
Characters fighting with \emph{Stamina + Brawl} can spend a single success to use a bite attack in addition to any other effects.  The target resists the kiss with Willpower at difficulty 8 as usual.

If kindred want to attack with their fangs by ripping out a chunk of someone's flesh then this is simply a bite attack where someone accepts a +2 increase in difficulty in return for an additional 2 Damage.

\section{Summary}
\begin{enumerate}
\item{The aggressor names the battleground, either Physical, Social or Mental Traits.}
\item{The defender specifies an Attribute Ability which both will use during the conflict.}
\item{The aggressor specifies the combat's Ability.}
\item{Both characters can add a Bonus, either from equipment, weapons, or Backgrounds.  These Bonuses do not have to match.}
\item{Both characters roll against each other.  If one achieves more successes than the other then those successes can inflict penalties.}
\begin{itemize}
\item{Damage: Inflict a -1 penalty to future rolls.}
\item{Position: Inflict +1 difficulty.}
\item{Recovery: Remove one difficulty penalty.}
\item{Multitask: Inflict all difficulty and dice penalties on one additional opponent.}
\item{Other: Take a separate action, such as grabing an item or using a Discipline.}
\end{itemize}
\end{enumerate}
\end{document}

